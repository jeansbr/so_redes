Temas para realização de trabalho científico:

1) Modelo OSI
2) Novas tecnologias de redes sem fio
3) Sistemas operacionais para celulares(descrever pelo menos 3 S.O. diferentes)
4) Principais diferenças entre os vários sistemas operacionais(descrever pelo menos 3 S.O. diferentes)

Para todos os temas, utilizar no mínimo 2 referências bibliográficas e no máximo 4.

Objetivo: - treinar o senso crítico, pesquisa científica, descobertas inovadoras a partir do conhecimento adquirido com o estudo dos temas propostos.
          - preparar os alunos para o mercado de trabalho incitando a busca de informação.

Sugestões de sites para pesquisa de trabalhos científicos:
*   Biblioteca Digital de Teses e Dissertações da USP  www.teses.usp.br/
*   Presidência da República  www.presidencia.gov.br/
*   Google Livros  http://books.google.com.br/
*   Scientific Electronic Library Online - SciELO   www.scielo.br/
*   Programa Biblioteca Eletrônica   www.probe.br/
*   Pesquisa FAPESP - Fundação de Amparo à Pesquisa do Estado de São Paulo www.revistapesquisa.fapesp.br/
*   Banco Nacional de Desenvolvimento  www.bndes.gov.br
*   IBGE Instituto Brasileiro de Geografia e Estatística   www.ibge.gov.br/
*   Portal Domínio Público  www.dominiopublico.gov.br
*   Sebrae  www.sebrae.com.br/
*   Anped - Associação Nacional de Pós-Graduação e Pesquisa em Educação www.anped.org.br/
*   Anpad - Associação Nacional de Pós-Graduação E Pesquisa Em Administração www.anpad.org.br/
*   Portal Brasileiro da Informação Científica  http://acessolivre.capes.gov.br/ 
*   Sociedade Brasileira de Computação  www.sbc.org.br/
*   Boletim Jurídico  www.boletimjuridico.com.br/portal.asp
*   Associação Brasileira de  Engenharia de Produção  www.abepro.org.br